//-----------------------------------------------------------------
// SPI Master - MODE 0 - Enabling CS or NSS.
//--------------
module wb_SPI(
	input               clk,
	input               reset,
	// Wishbone bus
	input      [31:0]   wb_adr_i,
	input      [31:0]   wb_dat_i,
	output reg [31:0]   wb_dat_o,
	input      [ 3:0]   wb_sel_i,
	input               wb_cyc_i,
	input               wb_stb_i,
	output              wb_ack_o,
	input               wb_we_i,
	// SPI 
	output              spi_sck,
	output              spi_mosi,
	input               spi_miso,
	output				  spi_cs
);

// SPI engine
reg init;
reg rst;
reg[7:0] address;
reg[7:0] dataWr;
wire[7:0] dataRd;
wire done;

topSPI SPIcore(
	.clk(clk),			//input
	.init(init),			//input 
	.reset(rst),		//input
	.address(address),		//input [7:0]
	.dataWr(dataWr), 		//input [7:0]
	.MISO(spi_miso), 		//input
	.SCKclock(spi_sck), 	//output
	.dataRd(dataRd),		//output [7:0]
	.MOSI(spi_mosi),			//output
	.NSS(spi_cs), 			//output
	.done(done)			//output
);

//---------------------------------------------------------------------------
// 
//------------------------------------------------




wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;
wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i;

reg  ack;
assign wb_ack_o = wb_stb_i & wb_cyc_i & ack;

always @(posedge clk)
begin
	if(reset)
	begin
		wb_dat_o[31:8] <= 24'b0;
		init <= 0;
		rst <= 1;
		ack <= 0;
	end
	else
	begin
		wb_dat_o[31:8] <= 24'b0;
		ack <= 0;
		init <= 0;
		//posible terminación, mejor dicho, init<-0
		//if(~done) --> meter los dos if(wb_rd y wr). else, init<-0
		if(wb_rd & ~ack & ~done)		//Read cycle
		begin
			ack <= 1;
			case(wb_adr_i[3:2])
				2'b00:begin
					wb_dat_o[31:8] <= 24'b0;
					wb_dat_o[7:0] <= dataRd;
					init <= 1;
				end
				2'b01: address <= wb_dat_i[7:0];
				default: begin
					wb_dat_o <= 32'b0;
					init <= 0;
				end
			endcase
		end
		else 
		begin 
			if(wb_wr & ~ack & ~done)	//Write cycle
			begin
				ack <= 1;
				case(wb_adr_i[3:2])
					2'b01: address <= wb_dat_i[7:0];
					2'b10: begin
						dataWr <= wb_dat_i[7:0];
						init <= 1;
					end
					default:begin
						address <= 8'b0;
						dataWr <= 8'b0;
						init <= 0;
					end
				endcase
			end
		end
	end
	
end

endmodule

	
	
	
